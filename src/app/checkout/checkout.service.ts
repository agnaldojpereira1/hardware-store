import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import { Observable } from 'rxjs';
import { nanoid } from "nanoid";

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private http: HttpClient) { }

  private  transaction_gateway_url = "https://transactiongateway.test.pago.dev";

  getQRCode(
        total_price,
        asset,
        receipt_message
    ): Observable<Object> {
        const amount = total_price.toPrecision(asset.decimals);
        return this.http.post("https://ipos-gateway.dev.pago.dev/payment-proxy/transaction-id", {
            "posId": "pos_1",
            "merchantId": "merch_1",
            "storeId": "store_1",
            "amount": amount,
            "assetId": 0,
            "receipt": receipt_message,
            "originator":"pagostore$pagoservices.com",
            "callbackUrl":"https://dejabit.com/transaction-status/123456789"
        });
    }

  checkoutProducts(
    pago_account_ids: String[],
    total_price,
    asset,
    receipt_message
  ): Observable<Object> {
    const transaction_id = uuid();
    const individualAmount = Number((Number(total_price) / pago_account_ids.length).toPrecision(asset.decimals));
    console.log(individualAmount);
    const transactions: Object = {};
    pago_account_ids.forEach((pago_account_id) => {
      const transaction_interface_id = uuid();
      transactions[transaction_interface_id] =  {
        type: 'Transfer',
        sender: pago_account_id,
        fee: 1000,
        amount: individualAmount,
        receiver: 'pagostore$pagoservices.com',
        lease: nanoid(32),
        assetId: asset.assetId,
        agreement: {
          id: transaction_id,
          type: 'text',
          description: 'Pago Store receipt',
          body: receipt_message
        }
      };
    });
    if (pago_account_ids.length === 1) {
      return this.http.post(`${this.transaction_gateway_url}/transactions/envelopes`, {
        name: transaction_id,
        originator: 'pagostore$pagoservices.com',
        description: 'Payment from Pago Store',
        sender: pago_account_ids[0],
        applicationId: 'pago_store',
        transaction: transactions[Object.keys(transactions)[0]],
      });
    } else {
      return this.http.post(`${this.transaction_gateway_url}/transactions/envelopes`, {
        name: transaction_id,
        description: 'Payment from Pago Store',
        originator: 'pagostore$pagoservices.com',
        applicationId: 'pago_store',
        transaction: {
          type: 'GroupTransaction',
          fee: 1000,
          sender: pago_account_ids[0],
          agreement: {
            id: transaction_id,
            type: 'text',
            description: 'Pago Store receipt',
            body: 'contents of receipt'
          },
          transactionInterfaceMapRequest: transactions,
        }
      });
    }
  }
}
