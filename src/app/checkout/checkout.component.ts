import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { CheckoutService } from './checkout.service';
import { AssetService } from '../assets/asset.service';
import { IAsset } from '../assets/asset';
import {IProduct} from '../products/product';

@Component({
  selector: 'pm-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  closeResult = '';
  @Input()
  checkout_total: string;
  pago_checkout: FormGroup;
  @Input()
  products: IProduct[];
  @Output()
  checkoutFinished = new EventEmitter<boolean>();
  @Output()
  qrcode_value: String;
  assets: IAsset[];

  constructor(
    private modalService: NgbModal,
    private checkoutService: CheckoutService,
    private assetService: AssetService,
    private fb: FormBuilder,
  ) {}

  open(content) {
    this.updateQRCode(() => {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        const asset_name = this.pago_checkout.get('asset_name').value;

        const chosenAsset = this.assets.find(
            (asset) => asset.unitName === asset_name
        );
        const receipt = this.buildReceiptMessage(chosenAsset);

        const payment_amount = Number(this.checkout_total) * Math.pow(10, chosenAsset.decimals);
        this.checkoutService.checkoutProducts(
            this.pago_checkout.get('pago_account_ids').value,
            payment_amount,
            chosenAsset,
            receipt,
        ).subscribe(
            (response) => {
              this.checkoutFinished.emit(true);
            },
            (error) => {
              this.checkoutFinished.emit(false);
            }
        );
      }, (reason) => {
        console.warn(`Cancelled checkout ${this.getDismissReason(reason)}`);
      });
    });
  }

  buildReceiptMessage(chosenAsset): string {
    let receipt_message = '';
    this.products.forEach(item => {
      receipt_message += `${item.productName}: ${item.price} ${chosenAsset.unitName}`;
    });
    return receipt_message;
  }

  ngOnInit(): void {
    this.qrcode_value = 'test';
    this.assetService.getAsset().subscribe(
      (assetPage) => {
        this.assets = assetPage.contents ? assetPage.contents : [];
        console.log(this.assets);
      },
      (error) => {
        this.assets = [];
        console.log(error);
      }
    );
    this.pago_checkout = this.fb.group({
      pago_account_ids: this.fb.array([
        this.fb.control('', Validators.required),
      ]),
      asset_name: ['ALGO', Validators.required]
    });
  }

  updateQRCode(callback): void {
    const asset_name = this.pago_checkout.get('asset_name').value;
    const chosenAsset = this.assets.find(
        (asset) => asset.unitName === asset_name
    );
    const payment_amount = Number(this.checkout_total) * Math.pow(10, chosenAsset.decimals);
    const receipt = this.buildReceiptMessage(chosenAsset);
    this.checkoutService.getQRCode(payment_amount, chosenAsset, receipt).subscribe((response) => {
      this.qrcode_value = response['qrCode'];
      callback();
    });
  }

  get pagoIds() {
    return this.pago_checkout.get('pago_account_ids') as FormArray;
  }

  addPagoId(): void {
    this.pagoIds.push(this.fb.control(''));
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
